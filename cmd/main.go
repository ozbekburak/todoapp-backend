package main

import (
	"flag"

	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"github.com/rs/cors"
	"gitlab.com/ozbekburak/todo-backend/api"
)

func main() {

	address := flag.String("addr", ":4000", "HTTP network address")

	flag.Parse()

	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	router := mux.NewRouter()
	router.HandleFunc("/list", api.ListTodos).Methods("GET", "OPTIONS")
	router.HandleFunc("/add", api.AddTodo).Methods("POST", "OPTIONS")

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"http://159.89.100.53:8080", "http://localhost:8080", "http://159.89.100.53:80"},
		AllowCredentials: true,
		AllowedMethods:   []string{"POST", "OPTIONS", "GET", "PUT", "DELETE", "HEAD"},
	})

	infoLog.Printf("Starting server on %s", *address)
	handler := c.Handler(router)
	errorLog.Fatal(http.ListenAndServe(*address, handler))
}
