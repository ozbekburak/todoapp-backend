module gitlab.com/ozbekburak/todo-backend

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.4
	github.com/rs/cors v1.8.0
	github.com/spf13/viper v1.9.0
	github.com/stretchr/testify v1.7.0
)
