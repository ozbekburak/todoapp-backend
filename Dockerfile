FROM golang:1.16-alpine

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . ./

COPY cmd /app/
RUN go build -o /todo-backend

EXPOSE 4000

CMD [ "/todo-backend" ]