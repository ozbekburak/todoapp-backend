# Todo Application (Go + VueJS + PostgreSQL)

# Contents

1. [Goal](#goal)
2. [Demo](#demo)
3. [Requirements](#requirements)
4. [Explanations](#explanations)
5. [Running in Docker](#installation)
6. [Running tests](#tests)
7. [API Documentation](#apidoc)


## Goal <a name="goal"></a>

Todo List Application including all the phases of software development project.

## Demo <a name="demo"></a>

You can watch the demonstration of the app:

![todoapp_ui_backend](https://gitlab.com/ozbekburak/todo-backend/-/tree/main/docs/todoapp_browser.gif)

![todoapp_docker_postman](https://gitlab.com/ozbekburak/todo-backend/-/tree/main/docs/todoapp.gif)

## Requirements <a name="requirements"></a>

- [X] Store the state of Todo list to a database (postgresql).
- [x] Written in "Go", "Vuejs".
- [X] Use protocols (http).
- [ ] TDD
    - [X] Component test
    - [X] Unit test
    - [ ] Integration test 
- [X] Dockerize frontend
- [X] Dockerize backend
- [ ] Deploy application
- [ ] Automate via CI/CD pipeline
    - [X] build and test nearly works :)
    - [ ] dockerize and deploy steps are not :(

## Explanations <a name="explanations"></a>

As a Go developer although I tried to complete every step, I had some shortcomings.

*   Could not fully implement A-TDD approach.
    *   Wrote unit tests for Go but I may not have provided all the coverage.
    *   Wrote component tests for VueJS, but I didn't have time to create a mock service or contract testing.

but;

*   Tried to implement all the processes, from developing frontend and backend to creating database and dockerizing them.
*   Uploaded the docker images I created to dockerhub for flexible use.
*   Created the docker-compose file to run it comfortably.
*   Created a pipeline with the build and test parts. Although I couldn't automate the Dockerize and Deployment parts, I showed them it on the pipeline with echo.

## Running in Docker <a name="installation"></a>

```bash
    git clone https://gitlab.com/ozbekburak/todo-backend.git
    cd todo-backend
    docker compose up --build
```

## Running tests <a name="tests"></a>

```bash
    git clone https://gitlab.com/ozbekburak/todo-backend.git
    cd frontend
    npm run test:unit
```

```go
    git clone https://gitlab.com/ozbekburak/todo-backend.git
    cd api
    go test -v
```

## API Documentation <a name="apidoc"></a>

You can find the api documentation [here](https://documenter.getpostman.com/view/15927024/UVJYHy4Q)