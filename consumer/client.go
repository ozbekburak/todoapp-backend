package consumer

import (
	"net/url"

	"gitlab.com/ozbekburak/todo-backend/model"
)

type Client struct {
	BaseURL *url.URL
}

// GetTodos gets a todo list from the API
func (c *Client) GetTodos(todoName string) (*model.Todo, error) {
	return &model.Todo{ID: 1, Todo: todoName}, nil
}
