package consumer

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/ozbekburak/todo-backend/model"
)

func TestClientUnit_GetTodos(t *testing.T) {
	todoName := "pact test"
	// setup mock server
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		assert.Equal(t, req.URL.String(), "/list")
		todos, _ := json.Marshal(model.Todo{
			Todo: todoName,
		})
		rw.Write([]byte(todos))
	}))
	defer server.Close()

	// setup client

	u, _ := url.Parse(server.URL)
	client := &Client{
		BaseURL: u,
	}
	todos, err := client.GetTodos(todoName)
	assert.NoError(t, err)

	assert.Equal(t, todos.Todo, todoName)

}
