package api

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func Router() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/list", ListTodos).Methods("GET")
	router.HandleFunc("/add", AddTodo).Methods("POST")
	return router
}

func TestEmptyTable(t *testing.T) {
	clearTable()

	request, _ := http.NewRequest("GET", "/list", nil)
	response := httptest.NewRecorder()
	Router().ServeHTTP(response, request)
	errorMessage := "{\"no rows found!\"}" //	TODO: İyi bir gösterim değil değiştir.
	if body := response.Body.String(); body != errorMessage {
		t.Errorf("Expected %s. Got %s", errorMessage, body)
	}
}

func TestListTodos(t *testing.T) {
	request, err := http.NewRequest("GET", "/list", nil)
	require.NoError(t, err)
	require.NotEmpty(t, request)

	response := httptest.NewRecorder()
	Router().ServeHTTP(response, request)

	assert.Equal(t, 200, response.Code, "OK response is expected (200 - OK)")
}

func TestAddTodo(t *testing.T) {
	type test struct {
		todo []byte
		want string
	}
	tests := []test{
		{
			todo: []byte(`{"todo":"buy some milk"}`),
			want: "buy some milk",
		},
		{
			todo: []byte(`{"todo":""}`),
			want: "can not be empty todo!",
		},
	}

	for _, testCase := range tests {
		request, err := http.NewRequest("POST", "/add", bytes.NewBuffer(testCase.todo))
		request.Header.Set("Content-Type", "application/json")

		require.NoError(t, err)
		require.NotEmpty(t, request)

		response := httptest.NewRecorder()
		Router().ServeHTTP(response, request)

		assert.Equal(t, testCase.want, strings.Trim(response.Body.String(), "\""), "Response body is expected!")
	}
}

func clearTable() {
	db := OpenDB()
	db.Exec("delete from todos")
	db.Exec("alter sequence todos_id_seq restart with 1")
}
