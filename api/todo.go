package api

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	_ "github.com/lib/pq"
	"gitlab.com/ozbekburak/todo-backend/model"
)

const (
	DB_USER     = "postgres"
	DB_PASSWORD = "12345"
	DB_NAME     = "todos"
	DB_PORT     = "5432"
	DB_HOST     = "localhost" // for docker-compose change to db
)

type MockTodo struct {
	ID   int    `json:"id"`
	Todo string `json:"todo"`
}

func ListTodos(w http.ResponseWriter, r *http.Request) {
	db := OpenDB()

	getStatement := `select id, todo from todos`
	rows, err := db.Query(getStatement)
	if err != nil {
		log.Fatal(err)
	}

	defer rows.Close()

	var todos []model.Todo
	for rows.Next() {
		var todo model.Todo
		err = rows.Scan(&todo.ID, &todo.Todo)
		if err != nil {
			log.Fatal("error while scanning todos:", err)
		}
		todos = append(todos, todo)
	}
	todosBytes, _ := json.MarshalIndent(todos, "", "\t")
	if len(todos) == 0 {
		notodo := []byte(`{"no rows found!"}`)
		w.Write(notodo)
	} else {
		w.Write(todosBytes)
	}

	defer db.Close()
}

func AddTodo(w http.ResponseWriter, r *http.Request) {
	db := OpenDB()

	var todo model.Todo
	err := json.NewDecoder(r.Body).Decode(&todo)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if todo.Todo == "" {
		w.Write([]byte(`"can not be empty todo!"`))
		return
	}

	addStatement := `insert into todos (todo) values ($1)`
	_, err = db.Exec(addStatement, todo.Todo)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		panic(err)
	}

	w.WriteHeader(http.StatusCreated) // create işleminde 201 kullan.
	todoByte, _ := json.MarshalIndent(todo.Todo, "", "\t")
	w.Write(todoByte)
	//w.Write([]byte(todo.Todo))

	defer db.Close()
}

func OpenDB() *sql.DB {

	// read config file, we are not using .env for now!
	//config, err := util.LoadConfig()
	//if err != nil {
	//	log.Fatal("cannot load config:", err)
	//}

	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", DB_HOST, DB_PORT, DB_USER, DB_PASSWORD, DB_NAME)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}

	err = db.Ping()
	if err != nil {
		panic(err)
	}

	return db
}
